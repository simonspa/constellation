# Constellation

## Broad strokes idea
Constellation is a DAQ/SCADA system designed with stability, maintainability and  ease of use in mind. It integrates established protocols into a modular system with very few different types of interface components that can still cater to the diverse types of hardware systems used in detector testing. 

The target use case ranges from doing a lab test on a single device, to a slightly more complicated lab setup with several conditions sensors and supporting devices like a HV supply or stage motors, to a testbeam setup with for instance triggering devices and beam monitoring detectors added. The system aims to be simple enough to  allow a student with minimal experience in data acquistion to start collecting data from their device within the limited time frame set by the duration of a typical bachelor's project. 

### System building blocks
The basic component of the constellation is the Satellite, which interfaces to e.g. a detector.

##### def of Finite State Machine (FSM):
- has steady states and transitional states 
- only a given set of transitions is allowed 

#### Satellite:
- is an FSM, i.e., has a well-defined state at any given time 
- listens to transition commands 
- broadcasts state via heartbeat channel
- listens to heartbeats 
- goes into safe state if someone's heartbeat is missing
- possibility to ignore missing heartbeats via importance (from list in configuration)

#### Controller:
- has no state 
- sends state transition commands (e.g. READY --> RUN)
- listens to heartbeats 
- can go offline and come back later 
- can thus be an arbitrary number of instances
- is the user interface

#### Listener:
- has no state 
- completely "passive" component wrt system state 
- subscribes to satellite messages 
- example: a database used for a Grafana dashboard, log display 

#### Heartbeat:
- every satellite publishes state at some given, moderate, frequency 
- every satellite subscribes to everyone else 


------------------------

## System structure/philosophy 

- The satellites are the only required components in the constellation. A satellite could be the interface to a detector under study, a trigger, a high-voltage supply, a thermometer, etc. 
- The user interfaces to the system via a controller. The controller is only required for state transitions, such as loading configurations, starting or stopping data taking, etc. The system can continue in a steady state, or exit a transitional state, without the controller. 
- Information which is not DATA is communicated via broadcasts, where the two roles are PUBLISHER and SUBSCRIBER. Only information which has a subscriber gets published.
- MONITORING can consist of log messages or any fraction of (reconstructed) data `TODO: others?`. Reconstruction is the responsibility of the end user of the system. The log messages are labelled by severity. 
- A LISTENER is only subscribing to MONITORING messages. A subscription is defined as all messages satisifying a list of specifications on the form listed under Message protocol below, from a given satellite. Each subscription becomes a single continuous stream, e.g., combining FATAL and DEBUG if those are both listed in the subscription. A subscriber can have several subscriptions, e.g. to separate FATAL from DEBUG from the same satellite.
- Each satellite broadcasts/publishes its state as a HEARTBEAT. All other satellites subscribe to its heartbeat broadcasts.
- If a satellite fails to detect the heartbeat of others, or it detects SAFE or ERROR from either of them (with an exception listed below), it goes to the SAFE state. If a satellite detects a fatal problem internally, it goes into ERROR. These are the only state transitions that a satellite is allowed to trigger by itself. All others are requested by the controller. 
- An ERROR, SAFE or missing heartbeat from a satellite that is marked as not important will not trigger others to go into SAFE state.
- The controller can issue a RESET from the ERROR or SAFE states.
- It is the end user's responsibility to define what a SAFE state for the given device means, and which actions should be taken on a transition `to_safe`.

------------------------

### FSM implementation

**Steady states**: IDLE, INITIALIZED, _**STOPPED**_ (_to be implemented_), PREPARED, RUNNING, SAFE, ERROR

**Transitional states** : (DE-)INITIALIZING, (UN)PREPARING 

[TODO: input chart from fsm.py here when there is an implementation of all states]

### Satellite implementation: 

A satellite has four streams of input/output, in order of importance: 
| QOS | stream |
| ------ | ------ |
| 0 | HEARTBEAT |
| 1 | COMMAND |
| 2 | DATA |
| 3 | MONITORING |

##### Satellite configuration
things to list:
- [ ] satellites important to this satellite 
- [ ] subscriptions 
- [ ] typical device parameter stuff like HV settings or whatnot 
-----------------------
### Message protocol


| blob | meaning |
| ------ | ------|
| LOG | log messages |
| STATS | data monitoring |
| LEVEL | log message severity level (FATAL/WARNING/INFO/DEBUG)       |
| variable |   monitoring variable e.g. temperature, nTriggers, ...     |
| component | `optional` satellite sumcomponent (like subdetector)|

Subscription defined per publishing satellite as `LOG/LEVEL/COMPONENT` with each message on the form 

`{time: <time in UTC seconds..?>, sender: <satellite name>, message: <message> }`

or 

`STATS/{variable}` on the form 

`{time: <time in UTC seconds..?>, sender: <satellite name>, type: <int, float, array, 2D>, value: <value> }`

TODO: could also add a key for the type of value, e.g something in a time series, an aggregate/cumulative, etc. 

-----------------------
### Network discovery of satellites 
[TODO input description]

-----------------------
### Heartbeat
Periodic broadcast of state, to allow detection of and subsequent graceful responses to system component failures without a central surveillance unit in place. The granularity of important/not important detectors allows keeping track of for instance _when_ a non-important detector (e.g. a temperature sensor) had a failure, while still upholding data taking. For this reason, it is the subscribing satellite which needs to know what other satellites are important to it. This information is part of the satellite configuration and is distributed when the controller requests initialization. This is a list of at most all the satellites discovered on the network. 
