import logging
import threading

import zmq
import msgpack

HB_INIT_LIFES = 3
HB_PERIOD = 1000

class HeartbeatChecker:
    def __init__(self, satellite) -> None:
        self.satellite = satellite
        self.sockets = list[zmq.Socket]()
        self.lifes = list[int]()
        self.interrupt_lock = threading.Lock()
        self.interrupted = False

    def register_check(self, ip: str, port: int) -> None:
        socket = self.satellite.context.socket(zmq.SUB)
        socket.connect(f"tcp://{ip}:{port}")
        socket.setsockopt_string(zmq.SUBSCRIBE, '')
        socket.setsockopt(zmq.RCVTIMEO, int(1.5 * HB_PERIOD))
        self.sockets.append(socket)
        self.lifes.append(HB_INIT_LIFES)
        logging.info(f'Registered heartbeating check for {ip}@{port}')

    def run_thread(self, index: int) -> None:
        logging.info(f'Thread {index} starting heartbeat check')
        socket = self.sockets[index]
        while self.satellite.do_heartchecking():
            try:
                message_bin = socket.recv()
                message = msgpack.unpackb(message_bin)
                state = message['state']
                logging.debug(f'Thread {index} got state {state}')
                if state == 'error' or state == 'safe':
                    # other satellite in error state, interrupt
                    self.interrupt(index)
                    break
                self.lifes[index] = HB_INIT_LIFES
            except zmq.error.Again:
                # no message after 1.5s, substract life
                self.lifes[index] -= 1
                logging.debug(f'Thread {index} removed life, now {self.lifes[index]}')
                if self.lifes[index] == 0:
                    # no lifes left, interrupt
                    self.interrupt(index)
                    break

    def interrupt(self, index: int) -> None:
        self.interrupt_lock.acquire()
        if not self.interrupted:
            self.interrupted = True
            self.satellite.Interrupt()
            logging.warning(f'Thread {index} interrupt from HeartbeatChecker')
        self.interrupt_lock.release()

    def run(self) -> None:
        self.interrupted = False
        threads = list[threading.Thread]()
        for index in range(len(self.sockets)):
            threads.append(threading.Thread(target=self.run_thread, args=(index,)))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()

class FakeSat:
    context = zmq.Context()
    def do_heartchecking(self) -> bool:
        return True
    def Interrupt(self) -> None:
        logging.error('Interrupted!')


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--log-level", default='debug')
    parser.add_argument("--ip", type=str, default='127.0.0.1')
    parser.add_argument("--port", type=int, default=61234)
    args = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(name)s: %(message)s",
        level=args.log_level.upper(),
    )

    hb_checker = HeartbeatChecker(FakeSat())
    hb_checker.register_check(args.ip, args.port)
    hb_checker.run()
