import time
from enum import Enum
import logging

import zmq
import msgpack

class DataType(Enum):
    INT = 1
    FLOAT = 2
    ARRAY2D = 3

class LogLevels(Enum):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR

class LogStatsPub:
    def __init__(self, context: zmq.Context, satellite_name: str, port: int) -> None:
        self.satellite_name = satellite_name
        # Create socket and bind wildcard
        self.socket = context.socket(zmq.PUB)
        self.socket.bind(f'tcp://*:{port}')

    def dispatch(self, topic: str, dict, value):
        self.socket.send_string(topic, zmq.SNDMORE)
        self.socket.send(msgpack.packb(dict), zmq.SNDMORE)
        self.socket.send(value)

    def sendStats(self, topic: str, type: DataType, value):
        statTopic = "STATS/" + topic
        dictData = {'time' : time.time(), 'sender' : self.satellite_name, 'type' : type.value}
        self.dispatch(statTopic, dictData, bytes(str(value), 'utf-8'))

    def sendLog(self, logLvl: LogLevels, message: str):
        topic = f'LOG/{logging.getLevelName(logLvl.value)}'
        dictData = {'time' : time.time(), 'sender' : self.satellite_name}
        logging.log(logLvl.value, message)
        self.dispatch(topic, dictData, bytes(message, 'utf-8'))
