import time
import zmq
import msgpack
import argparse


def receiver(args):
    context = zmq.Context()
    data_receiver = context.socket(zmq.PULL)
    data_receiver.connect(args.publisher)

    if args.file:
        file = open(args.file, "wb")

    while True:
        bin = data_receiver.recv()
        bin2 = data_receiver.recv()
        data = msgpack.unpackb(bin)

        if args.file:
            file.write(bin2);

        print("Received event ", data)


parser = argparse.ArgumentParser()
parser.add_argument("--file", type=str)
parser.add_argument("--publisher", type=str, required=True)
args = parser.parse_args()

receiver(args)