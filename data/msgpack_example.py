from msgpack_serialize import * 

"""
Users should be able to define their own datatype to be submitted!
One's DUT will probably output some bytestream, but for sensors
in a lab setting, it is nice to not have to write a serializer by
oneself.

This little class holds a single float, and inherits from dataAbstract

It has three abstract methods, i.e. they have to be overridden by
this derived class:
    1. __init__
    2. encode
    3. decode
"""
class singleFloat(dataAbstract):
    def __init__(self, _f):
        self.data = _f

    """
    msgpack can serialize any simple python types, structured as
    dicts or lists in whatever complicated structure. 

    msgpack is not a complete class serialization library however
    so we do have to create, by hand, these two encoder and decoder
    functions. Still, this is easier than trying to serialize into
    e.g. a std::vector<uint32_t>.
    """
    def encode(self):
        return [self.data]

    #note: no self argument
    def decode( _serializedData):
        return singleFloat(_serializedData[0])

#Just another example of a data class,
#with a hand-crafted encoder and decoder
class temperatureMeasurement(dataAbstract):
    def __init__(self, _temp, _humidity, _quality):
        self.temp = _temp
        self.humidity = _humidity
        self.quality = _quality

    def encode(self):
        return [self.temp, self.humidity, self.quality]

    def decode(_data):
        return temperatureMeasurement(_data[0], _data[1], _data[2])
        

#Example data
data = singleFloat(15.0)
#There is also a super simple header class
h = eventHeader(10,20)
#and an event is a header + data, simple enough for now
e = event(h, data)

#Serializing event, note the "default=serialize_event"
packed_event = msgpack.packb(e, default=serialize_event)
print(packed_event)
#deserialize the event
unpacked_event = msgpack.unpackb(packed_event, object_hook=deserialize_event)
#And boom, the data is recovered!
print(unpacked_event)
print(unpacked_event.header.eventID)
print(unpacked_event.data.data)
