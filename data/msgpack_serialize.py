import msgpack
from abc import ABC, ABCMeta, abstractmethod
import importlib

#Event header
class eventHeader:
    def __init__(self, _timestamp, _eventID):
        self.timestamp = _timestamp
        self.eventID = _eventID

#Abstract data class, where all member functions have to
#be overriden by a derived class
class dataAbstract(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def encode(self):
        pass

    @abstractmethod
    def decode(_serializedData):
        pass

class event:
    def __init__(self, _header, _data):
        self.header = _header
        self.data = _data

def deserialize_event(obj):
    if '__event__' in obj:
        print(obj)
        header = eventHeader(obj['timestamp'], obj['eventID'])
        module = importlib.import_module(obj['__data_module__'])
        data = getattr(module, obj['__data_name__']).decode(obj['__data_content__'])
        return event(header, data)

def serialize_event(obj):
    if isinstance(obj, event):
         return {'__event__' : True, 'timestamp': obj.header.timestamp, 'eventID' : obj.header.eventID, '__data_module__': type(obj.data).__dict__['__module__'], '__data_name__' : type(obj.data).__name__, '__data_content__': obj.data.encode()}
     
    return obj

