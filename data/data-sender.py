import time
import zmq
import msgpack
import os
import argparse

def producer(args):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.bind("tcp://*:" + str(args.port))

    payload = os.urandom(args.bytes)

    # Start your result manager and workers before you start your producers
    t0 = time.time()
    for num in range(0,args.events):
        data = { 'eventid' : num, 'time' : time.time() }
        bin = msgpack.packb(data)
        zmq_socket.send(bin, zmq.SNDMORE)
        zmq_socket.send(payload)

    t1 = time.time()
    print(f'total time for {args.events} evt / {args.events * args.bytes / 1024 / 1024}MB: {t1 - t0}s')


parser = argparse.ArgumentParser()
parser.add_argument("--port", type=int, required=True)
parser.add_argument("--bytes", type=int, default=1024)
parser.add_argument("--events", type=int, default=2000000)
args = parser.parse_args()

producer(args)
