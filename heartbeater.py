import time
import threading

import zmq
import msgpack

HB_PERIOD = 1000

class Heartbeater:
    def __init__(self, satellite, port: int) -> None:
        self.satellite = satellite
        self.socket = satellite.context.socket(zmq.PUB)
        self.socket.bind(f'tcp://*:{port}')
        self.do_heartbeating = True

    def start(self):
        self.run_thread = threading.Thread(target=self.run)
        self.run_thread.start()

    def stop(self):
        self.do_heartbeating = False
        self.run_thread.join()

    def run(self) -> None:
        while self.do_heartbeating:
            state = self.satellite.get_state()
            dictData = {'time' : time.time(), 'state' : state}
            self.socket.send(msgpack.packb(dictData), zmq.NOBLOCK)
            time.sleep(HB_PERIOD*1e-3)

class FakeSat:
    context = zmq.Context()
    def get_state(self) -> str:
        return 'Fake'

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=61234)
    args = parser.parse_args()

    heartbeater = Heartbeater(FakeSat(), args.port)
    heartbeater.start()
    time.sleep(10)
    heartbeater.stop()
