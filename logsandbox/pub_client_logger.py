import sys
import zmq

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)
    
if len(sys.argv) > 2:
    port1 =  sys.argv[2]
    int(port1)

# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)

print("Collecting logs...")
socket.connect ("tcp://192.168.12.233:" + port)

if len(sys.argv) > 2:
    socket.connect (f"tcp://localhost:{port1}")

# Subscribe to all logs
#topicfilter = "LOG/WARNING"
topicfilter = "STATS"
socket.setsockopt_string(zmq.SUBSCRIBE, topicfilter)

# Process 5 updates
total_value = 0
for update_nbr in range (500):
    string = socket.recv()
    topic, messagedata = string.split()
    print(topic, messagedata)

      