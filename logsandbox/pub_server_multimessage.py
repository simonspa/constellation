import zmq
import random
import sys
import time
import msgpack
import datetime
import platform
import eddaEnums
from msghead import MsgHeader

onPi = ("rpi" in platform.platform())
if onPi:
    from gpiozero import CPUTemperature, LoadAverage

def dispatch(topic, dict, value):
    socket.send_string(topic, zmq.SNDMORE)
    socket.send(msgpack.packb(dict, default=MsgHeader.encode), zmq.SNDMORE)
    socket.send(value)

# Parameters: string for the topic
# Type of the value to send (enum)
# Value to send
def sendStats(topic, type, value):
    statTopic = "STATS/" + topic
    dictData = {'time' : time.time(), 'sender' : myName, 'type' : type.value}
    dispatch(statTopic, dictData, bytes(str(value), 'utf-8'))


# Parameters: string for the log level (topic of the PUB)
# String for the message
def sendLog(logLvl, message):
    topic = "LOG/" + logLvl
    dict = {'tag1': 'value1', 'tag2': 'value2'}
    dictData = MsgHeader(myName, msgpack.Timestamp.from_unix_nano(time.time_ns()), dict)
    dispatch(topic, dictData, bytes(message, 'utf-8'))
    print(f"{logLvl} {dictData.time} {dictData.sender} {message}")



port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

myName = platform.node() + ":" + port

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

while True:
    randomNumber = random.randrange(0,10)
    messagedata = random.randrange(1,215) - 80
    if(randomNumber > 6):
        sendLog("WARNING", "Hej, something is wrong with me, halp!")
        if(random.randrange(0,100)>=99):
            sendLog("ERROR", "Hjälp! Something is seriously wrong!")
    else:
        sendLog("DEBUG", "Hej, just FYI, this is hapening to me right now")
    time.sleep(1)
    if onPi:
        #CPU temp
        cpu = CPUTemperature()
        load = LoadAverage()
        #sendStats("TEMP", float, cpu.temperature)
        sendStats("TEMP", eddaEnums.DataType.FLOAT, cpu.temperature)
        sendStats("LOADAVG", eddaEnums.DataType.FLOAT, load.load_average)
