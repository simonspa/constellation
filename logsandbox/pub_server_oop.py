import zmq
import random
import sys
import time
import msgpack
import datetime
import platform
import eddaEnums
from loggerbase import LoggerBase

onPi = ("rpi" in platform.platform())
if onPi:
    from gpiozero import CPUTemperature, LoadAverage

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

logger = LoggerBase(port)

while True:
    randomNumber = random.randrange(0,10)
    messagedata = random.randrange(1,215) - 80
    if(randomNumber > 6):
        logger.sendLog("WARNING", "Hej, something is wrong with me, halp!")
        if(random.randrange(0,100)>=99):
            logger.sendLog("ERROR", "Hjälp! Something is seriously wrong!")
    else:
        logger.sendLog("DEBUG", "Hej, just FYI, this is hapening to me right now")
    time.sleep(1)
    if onPi:
        #CPU temp
        cpu = CPUTemperature()
        load = LoadAverage()
        #sendStats("TEMP", float, cpu.temperature)
        logger.sendStats("TEMP", eddaEnums.DataType.FLOAT, cpu.temperature)
        logger.sendStats("LOADAVG", eddaEnums.DataType.FLOAT, load.load_average)
