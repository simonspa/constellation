import msgpack

class MsgHeader:
    def __init__(self, sender, timestamp: msgpack.Timestamp, tags: dict):

        self.sender = sender
        self.time = timestamp
        self.tags = tags

    def time_ns(self): return self.time.to_unix_nano()

    def time_s(self): return self.time.to_unix()

    def encode(_obj):
        return [_obj.sender, _obj.time, _obj.tags]

    def decode(_data):
        return MsgHeader(_data[0], _data[1], _data[2])

