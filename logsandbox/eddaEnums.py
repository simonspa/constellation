from enum import Enum
import logging

class DataType(Enum):
    INT = 1
    FLOAT = 2
    ARRAY2D = 3

class LogLevels(Enum):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARN
    ERROR = logging.ERROR