import zmq
import argparse



# arguments like port and IP

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--ip", help="IP address for the publisher", default="127.0.0.1")
parser.add_argument("-p", "--port", help="port number", default="5680")
args = parser.parse_args()

ip=args.ip
port=args.port
logaddress=f'tcp://{ip}:{port}'


# ZeroMQ subscriber context
context = zmq.Context()
sock = context.socket(zmq.SUB)

# Define subscription and messages with prefix to accept.
sock.setsockopt_string(zmq.SUBSCRIBE, "log/INFO")


sock.connect(logaddress)
print(f'Creating subscriber at {logaddress}')

while True:
    message= sock.recv()
    print (message)
