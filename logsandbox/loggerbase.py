import zmq
import random
import sys
import time
import msgpack
import datetime
import platform
import logsandbox.eddaEnums

class LoggerBase:
    def __init__(self, log_port):
        self.name = platform.node() + ":" + str(log_port)

        self.zmq_context = zmq.Context()
        self.socket = self.zmq_context.socket(zmq.PUB)
        self.socket.bind("tcp://*:%s" % log_port)
        

    def dispatch(self, topic, dict, value):
        self.socket.send_string(topic, zmq.SNDMORE)
        self.socket.send(msgpack.packb(dict), zmq.SNDMORE)
        self.socket.send(value)

    # Parameters: string for the topic
    # Type of the value to send (enum)
    # Value to send
    def sendStats(self, topic, type, value):
        statTopic = "STATS/" + topic
        dictData = {'time' : time.time(), 'sender' : self.name, 'type' : type.value}
        self.dispatch(statTopic, dictData, bytes(str(value), 'utf-8'))


    # Parameters: string for the log level (topic of the PUB)
    # String for the message
    def sendLog(self, logLvl, message):
        topic = "LOG/" + logLvl
        dictData = {'time' : time.time(), 'sender' : self.name}
        self.dispatch(topic, dictData, bytes(message, 'utf-8'))
        print(f"{logLvl} {dictData} {message}")
