import sys
import zmq
import requests
import argparse
import msgpack
import time
import msgpack
import time

parser = argparse.ArgumentParser()
parser.add_argument("--url", type=str, required=True)
parser.add_argument("--publisher", action='append')
parser.add_argument("--topic", action='append')
parser.add_argument("--local", action='store_true')
args = parser.parse_args()

# Socket to talk to server
print("Starting up, opening sockets...")
context = zmq.Context()
poller = zmq.Poller()

# Register all publishers we should be listening to:
for pub in args.publisher:
    print("Connecting to ", pub)
    socket = context.socket(zmq.SUB)
    socket.connect(pub)
    poller.register(socket, zmq.POLLIN)

    # Subscribe to all requested topics:
    print("Subscribing to topics...")
    for topic in args.topic:
        socket.setsockopt_string(zmq.SUBSCRIBE, topic)

# Receive logs:
print("Starting listening to logs...")
while True:
    socks = dict(poller.poll())

    for sock, msg in socks.items():

        # Get topic of this log
        topic = sock.recv()
        # Get message header with metadata
        binheader = sock.recv()
        header = msgpack.unpackb(binheader)
        # Get message
        message = sock.recv()

        timestamp = time.strftime("%H:%M:%S", time.gmtime(header.get("time")))

        if args.local:
            print(topic, header, message)

        # Send log to Mattermost
        reqheaders = {'Content-Type': 'application/json',}
        reqvalues = '{ "text": "**' + header.get("sender") + '** says at ' + timestamp + ' that:\n**' + topic.decode('UTF-8') + '**: ' + message.decode('UTF-8') + '"}'
        response = requests.post(args.url, headers=reqheaders, data=reqvalues.encode('utf-8'))


      
