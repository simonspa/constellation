import zmq
import time
import datetime
import psutil # to provide sensor function
import argparse


def prepare_log(logaddress="tcp://127.0.0.1:5680"):
    # ZeroMQ context to publish messages
    context = zmq.Context()
    global logsock
    logsock = context.socket(zmq.PUB)
    logsock.bind(logaddress)
    print(f'Creating publisher at {logaddress}')


def publish_log(message, socket, level="INFO"):
    logstring="log/"+level+" "+message
#    print(logstring)
    socket.send_string(logstring)


# arguments like port and IP

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--ip", help="IP address for the publisher", default="127.0.0.1")
parser.add_argument("-p", "--port", help="port number", default="5680")
args = parser.parse_args()

ip=args.ip
port=args.port
logaddress=f'tcp://{ip}:{port}'

# main code
prepare_log(logaddress)

cputemp = 0
if not hasattr(psutil, "sensors_temperatures"):
    sys.exit("platform not supported")
    temps = psutil.sensors_temperatures()


count = 0
while True:
    time.sleep(1)
    count+=1
    temps, dtnow = psutil.sensors_temperatures(), datetime.datetime.now()
    if not temps:
        sys.exit("can't read any temperature")
    firsttemp=list(temps.values())[0]
    secondtemp=list(temps.values())[1]
    publish_log(f'frequent temp reading is {firsttemp} at {dtnow}', logsock, level="DEBUG")
    if count % 10 == 0:
        publish_log(f'occasional reading is {secondtemp} at {dtnow}', logsock)
