import zmq
import random
import sys
import time

from gpiozero import CPUTemperature

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

while True:
    randomNumber = random.randrange(0,10)
    messagedata = random.randrange(1,215) - 80
    if(randomNumber > 6):
        topic = "WARNING"
        socket.send_string(f"LOG/{topic} {messagedata}")
        print(f"LOG/{topic} {messagedata}")
    else:
        topic = "DEBUG"
        socket.send_string(f"LOG/{topic} {messagedata}")
    time.sleep(1)
    
    #CPU temp
    cpu = CPUTemperature()
    tempTopic = "STATS/TEMP"
    socket.send_string(f"{tempTopic} {cpu.temperature}")

