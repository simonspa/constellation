import sys
import zmq
import msgpack
from eddaEnums import LogLevels, DataType
from termcolor import colored
import time
from msghead import MsgHeader


class LogListener:
    def __init__(self, args) -> None:
        # Context to talk to publishers over
        self.context = zmq.Context()
        self.poller = zmq.Poller()

        # Register all publishers we should be listening to:
        for pub in args.publisher:
            print("Connecting to", pub)
            socket = self.context.socket(zmq.SUB)
            if not "tcp://" in pub[:6]:
                pub = "tcp://" + pub
            socket.connect(pub)
            self.poller.register(socket, zmq.POLLIN)

            # Subscribe to all requested topics:
            print("Subscribing to topics", args.topic)
            for topic in args.topic:
                # Check for log levels
                if "LOG/" in topic:
                    mylevel = topic.rsplit('/')[1]
                    try:
                        myvalue = LogLevels[mylevel].value
                        for level in LogLevels:
                            if level.value >= myvalue:
                                socket.setsockopt_string(zmq.SUBSCRIBE, "LOG/" + level.name)
                    except KeyError:
                        print("Invalid log level ", mylevel)
                        pass
                else:
                    socket.setsockopt_string(zmq.SUBSCRIBE, topic)

        self.logfile = open(args.logfile, "w") if args.logfile else None
        self.csvfile = open(args.csvfile, "w") if args.csvfile else None

    def topic_color(self, topic):
        if "ERR" in topic:
            return "red"
        elif "WARN" in topic:
            return "light_red"
        elif "INFO" in topic:
            return "cyan"
        else:
            return "dark_grey"

    def receive(self, socket):
        try:
            head = MsgHeader.decode(msgpack.unpackb(socket.recv()))
            msg = socket.recv()
            return head, msg
        except msgpack.exceptions.ExtraData:
            pass

    def printLog(self, topic, header, msg):
        timestamp = colored(time.strftime("%H:%M:%S", time.gmtime(header.time_s())), "light_grey", attrs=["bold"])
        level = colored(topic[4:].rjust(7), self.topic_color(topic), attrs=["bold"])
        sender = colored(header.sender.rjust(12), "light_blue")
        message = colored(msg.decode("utf-8"), "white")
        print(timestamp, level, sender, message)

    def printStats(self, topic, header, msg):
        timestamp = colored(time.strftime("%H:%M:%S", time.gmtime(header.get("time"))), "light_grey", attrs=["bold"])
        sender = colored(header.get("sender").rjust(20), "light_blue")
        name = topic[6:]
        dtype = "(" + str(DataType(header.get("type"))) + ")"
        if(header.get("type") == DataType.FLOAT.value):
            value = float(msg)
        print(timestamp, sender, name, "=", value, dtype)

    def run(self):
        """Main logging routine"""
        while True:
            socks = dict(self.poller.poll())
            for sock, items in socks.items():
                for _ in range(items):
                    topic = sock.recv().decode("utf-8")
                    head, msg = self.receive(sock)

                    if(topic.startswith("LOG")):
                        self.printLog(topic, head, msg)
                        if self.logfile:
                            self.logfile.write(f"{topic}, {head}, {msg}\n");
                    elif(topic.startswith("STAT")):
                        self.printStats(topic, head, msg)
                        if self.csvfile:
                            self.csvfile.write(f'{time.strftime("%H:%M:%S", time.gmtime(head.get("time")))},{topic[6:]},{msg}\n')
                    else:
                        print("Unknown topic...")

def main(args=None):
    """Start the LogListener"""
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--publisher", "--pub", action='append')
    parser.add_argument("--topic", action='append')
    parser.add_argument("--logfile", type=str)
    parser.add_argument("--csvfile", type=str)
    args = parser.parse_args(args)

    # start server with remaining args
    ll = LogListener(args)
    ll.run()


if __name__ == "__main__":
    main()



