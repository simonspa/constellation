import sys
import zmq
import requests
import argparse
import msgpack
import time
import msgpack
import time
import logging
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--publisher", action='append')
parser.add_argument("--topic", default="STAT")
parser.add_argument("--local", action='store_true')
parser.add_argument("--basename", default='')
parser.add_argument("--ncolumns", default=20)
args = parser.parse_args()

timestr = time.strftime("%Y%m%d-%H%M%S")
logfilename=f'{args.basename}/log_{timestr}.csv'

# Socket to talk to server
print("Starting up, opening sockets...")
context = zmq.Context()
poller = zmq.Poller()

# Register all publishers we should be listening to:
for pub in args.publisher:
    print("Connecting to ", pub)
    socket = context.socket(zmq.SUB)
    socket.connect(pub)
    poller.register(socket, zmq.POLLIN)

    # Subscribe to all requested topics:
    print("Subscribing to topics...")
    for topic in args.topic:
        socket.setsockopt_string(zmq.SUBSCRIBE, topic)

# some preparations for the logging
topicList = []
logging.basicConfig(filemode = 'a', level=logging.DEBUG)
csvlog = logging.getLogger('csvlog') # init the logger
csvlog.setLevel(logging.DEBUG) # capture everything
csvhandler = logging.FileHandler(logfilename, 'a+')
csvlog.addHandler(csvhandler)
stdout = logging.StreamHandler(stream=sys.stdout)
if args.local:
    csvlog.addHandler(stdout)

# Receive logs:
headerline='timestamp'+int(args.ncolumns)*','
csvlog.info(headerline)
print(f"{logfilename} created.")
print("Starting listening to logs...")
while True:
    socks = dict(poller.poll())

    for sock, msg in socks.items():

        # Get topic of this log
        topic = sock.recv()
        # Get message header with metadata
        binheader = sock.recv()
        header = msgpack.unpackb(binheader)
        # Get message
        message = sock.recv()

        sender=header.get("sender")
        #filter out special characters
        nicesender="".join(ch for ch in sender if ch.isalnum())
        subtopic=topic.decode('UTF-8').rsplit('/')[-1]
        category=subtopic+'_'+nicesender
        # for new topics, create new log file hander
        # e.g. STAT/TEMP from eddaB:5556 -> category TEMP_eddaB5556
        if category not in topicList:
            topicList.append(category)
            nextcolumn=headerline.find(',,')+1
            if nextcolumn == -1:
                headerline=headerline+category
            else:
                headerline=headerline[:nextcolumn]+category+headerline[nextcolumn:]
            if args.local:
                print(headerline)
            cmd = '1 s/^.*/' + headerline + '/g'
            csvlog.removeHandler(csvhandler)
            csvhandler.close()
            subprocess.call(['sed', '-i', '-e', cmd, logfilename])
            csvhandler = logging.FileHandler(logfilename, 'a+')
            csvlog.addHandler(csvhandler)

        timestamp=header.get("time")
        itopic=topicList.index(category)
        logline=f'{timestamp}'+(1+itopic)*','+message.decode('UTF-8')+(int(args.ncolumns)-itopic-1)*','            
        csvlog.info(logline)
        
