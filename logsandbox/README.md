# Logging and sending statistics

Using the PUB/SUB system from ZeroMQ.

We want a publisher, which will be a part of each satellite. PUB/SUB works using **topics**. These are the first part of a published message. The subsciber will only receive messages of a defined topic, which can be set on the subscriber socket by `socket.setsockopt(zmq.SUBSCRIBE, topicfilter))`.

We will have topics for different log levels, and topics for different statistics quantities.

A Publisher doesn't publish unless there is a subscriber.

The files `pub_server_multimessage.py` and `sub_client_multimessage.py` hold a basic example of how it can work. The publisher does a `WARNING` log message if a random number is larger than 6, and a `DEBUG` log message otherwise. It also always provides a "temperature" `STATS/TEMP` message.

Each subscriber needs to connect with a socket to a publisher, and subscribe to one or more topics.

The `sub_client_multimessage.py` program takes the command line arguments `--publisher <ip:port>` and `--topic <topic>`. Multiple flags can be given to subscribe to several publishers and several topics.

## Logging

Each message sent byt he publishing socket will have three parts:
* The log level (i.e. the topic of the message) given as `LOG/LEVEL(/COMPONENT)`
* A dictionary containing the time of the message, and the name of the sender ({time : <...>, sender : <...>})
* A string containing the log message

The log levels are TRACE, DEBUG, INFO, WARNING, ERROR, STATUS. Through the COMPONENT, the subscription can be further specialised, so as not to get all the output from e.g. DEBUG.

The dictionary is serialised, using for example `msgpack`, before being sent using ZeroMQ.

## Statistics

Each message sent by the publishing socket will have three parts:
* The quantity provided (i.e. the topic of the message) given as `STATS/<VAR>`, where `VAR` can be e.g. `TEMP` 
* A dictionary containing the time of the message, the name of the sender, and the variable type ({time : <...>, sender : <...>, type : <...>})
* The value of the quantity

The variable type can be `int`, `float`, `array2D` (for now; basic types). The goal is to be able to use this output to easily plot using e.g. Grafana.

The dictionary is serialised, using for example `msgpack`, before being sent using ZeroMQ.