#include <chrono>
#include <map>
#include <variant>

#include <msgpack.hpp> // https://github.com/msgpack/msgpack-c/blob/cpp_master/

class MessageHeader {
public:
    MessageHeader(const std::string sender, std::chrono::system_clock::time_point time) : sender_(sender), time_(time) {}
    MessageHeader(const std::string sender) : sender_(sender), time_(std::chrono::system_clock::now()) {}
    MessageHeader() : time_(std::chrono::system_clock::now()) {};

    std::chrono::system_clock::time_point getTimestamp() const { return time_; }
    time_t getTime() const {
        return std::chrono::system_clock::to_time_t(time_);
    }
    std::string getSender() const { return sender_; }

    template<typename T>
    T getTag(const std::string& key) const { return T{}; }

    template<typename T>
    void setTag(const std::string& key, T value) { tags_[key] = value; }

    MSGPACK_DEFINE(sender_, time_, tags_)
private:
    std::string sender_{};
    std::chrono::system_clock::time_point time_{};
    std::map<std::string, msgpack::type::variant> tags_{};
};


template<>
std::string MessageHeader::getTag<>(const std::string& key) const { return tags_.at(key).as_string(); }
