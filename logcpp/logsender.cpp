#include <chrono>
#include <ctime>
#include <iostream>
#include <map>
#include <variant>
#include <thread>

#include <boost/asio/ip/host_name.hpp>
#include <zmq.hpp> // https://github.com/zeromq/cppzmq
#include <msgpack.hpp> // https://github.com/msgpack/msgpack-c/blob/cpp_master/

#include "msghead.hpp"

int main(int, const char**) {

    std::cout << "msgpack v" << MSGPACK_VERSION << std::endl;

    //  Prepare our context and subscriber
    zmq::context_t context(1);
    zmq::socket_t publisher(context, zmq::socket_type::pub);
    publisher.bind("tcp://*:5556");

    std::cout << "Starting to send stuff" << std::endl;
    while (1) {

        // Send topic
        publisher.send(zmq::str_buffer("LOG/DEBUG"), zmq::send_flags::sndmore);

        // Prepare header
        auto msghead = MessageHeader(boost::asio::ip::host_name());
        msghead.setTag("tag1", "value1");

        // Pack and send header
        std::stringstream header;
        msgpack::pack(header, msghead);
        zmq::message_t header_frame(header.str().c_str(), header.str().length());
        publisher.send(header_frame, zmq::send_flags::sndmore);

        // Send message
        publisher.send(zmq::str_buffer("This is the payload and it's incredibly boring"));

        // Wait a bit.
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    return 0;
}
