#include <chrono>
#include <ctime>
#include <iostream>
#include <map>
#include <variant>

#include <zmq.hpp> // https://github.com/zeromq/cppzmq
#include <msgpack.hpp> // https://github.com/msgpack/msgpack-c/blob/cpp_master/

#include "msghead.hpp"

int main(int argc, const char* argv[]) {
    int return_code = 0;
    if(argc == 1) {
        return_code = 1;
    }

    std::vector<std::string> verbosity_levels;
    for(int i = 1; i < argc; i++) {
        if(strcmp(argv[i], "-o") == 0 && (i + 1 < argc)) {
            verbosity_levels.emplace_back(std::string(argv[++i]));
            std::cout << "Listening to messages with level " << verbosity_levels.back() << std::endl;
        } else {
            std::cout << "Unrecognized command line argument \"" << argv[i] << "\"" << std::endl;
            return_code = 1;
        }
    }

    if(return_code) {
        std::cout << "Exiting, check command line arguments" << std::endl;
        return return_code;
    }


    std::cout << "msgpack v" << MSGPACK_VERSION << std::endl;

    //  Prepare our context and subscriber
    zmq::context_t context(1);
    zmq::socket_t subscriber(context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5556");

    for(auto& level : verbosity_levels) {
        subscriber.set(zmq::sockopt::subscribe, level);
    }

    std::cout << "Starting to listen" << std::endl;
    while (1) {

        // We expect three parts:
        zmq::message_t message_topic;
        auto retval = subscriber.recv(message_topic);
        std::cout << "Got topic" << std::endl;
        zmq::message_t message_header;
        retval = subscriber.recv(message_header);
        std::cout << "Got header" << std::endl;
        zmq::message_t message_payload;
        retval = subscriber.recv(message_payload);
        std::cout << "Got payload" << std::endl;

        // Read envelope with log level
        auto topic = std::string(static_cast<char*>(message_topic.data()), message_topic.size());

        // Read header class
        auto header = msgpack::unpack(static_cast<const char*>(message_header.data()), message_header.size());

        // Generate object from handle
        msgpack::object deserialized = header.get();
        // This supports ostream - nice
        std::cout << deserialized << std::endl;

        // Convert object to final type:
        auto msgheader = deserialized.as<MessageHeader>();
        auto tt = msgheader.getTime();
        auto sender = msgheader.getSender();

        // auto hdata = deserialized.as<std::map<std::string, msgpack::type::variant>>();
        // auto sender = hdata["sender"].as_string();
        // auto time = std::chrono::time_point<std::chrono::system_clock>(std::chrono::nanoseconds(hdata["time"].as_uint64_t()));
        // auto tt = std::chrono::system_clock::to_time_t(time);

        auto message = std::string(static_cast<char*>(message_payload.data()), message_payload.size());

        std::cout << ctime(&tt) << "\t" << topic << "\t" << sender << "\t" << message << " -> " << msgheader.getTag<std::string>("tag1") << std::endl;
    }
    return 0;
}
